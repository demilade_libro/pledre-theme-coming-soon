window.pledreTheme = {
  auth: {
    isLoggedIn: isLoggedIn,
    getUserType: getUserType,
    getUserDetails: getUserDetails,
  },
  actions: {
    primary: {
      href: getAuthActionsPrimary().href,
      message: getAuthActionsPrimary().message,
    },
  },
  openChatWidget: openChatWidget,
  goToHashLocationFromAnchorTag: goToHashLocationFromAnchorTag,
};

function getAuthActionsPrimary() {
  //   if user us logged in it returns /dashboard as url else it returns /login
  //   It returns a urlMessage based on user type

  const userType = getUserType();
  let message;
  let href;

  switch (userType) {
    case "teacher":
      message = "Teacher console";
      href = "/dashboard";
      break;
    case "student":
      message = "Class console";
      href = "/dashboard";
      break;
    case "admin":
      message = "Admin dashboard";
      href = "/dashboard";
      break;
    default:
      message = "Login/Register";
      href = "/login";
  }

  return {
    message,
    href,
  };
}

function isLoggedIn() {
  localStorage.getItem("user_token") || localStorage.getItem("auth_token");
}

function getUserType() {
  const details = localStorage.getItem("user_details");
  return details ? JSON.parse(details).user_type : undefined;
}

function getUserDetails() {
  const details = localStorage.getItem("user_details");
  return details ? JSON.parse(details) : undefined;
}

function openChatWidget() {
  var done = false;
  var script = document.createElement("script");
  script.async = true;
  script.type = "text/javascript";
  script.src = "//js.hs-scripts.com/5826839.js";
  document.getElementsByTagName("HEAD").item(0).appendChild(script);
  script.onreadystatechange = script.onload = function (e) {
    if (
      !done &&
      (!this.readyState ||
        this.readyState == "loaded" ||
        this.readyState == "complete")
    ) {
      done = true;
    }
  };
}

function goToHashLocationFromAnchorTag(hashLocation) {
  //   Working with base href causes issue if you navigate to a hash e.g #categories. THe browser would navigate to base_href/#categories which ends up being https://assets.media.pledre.com/#categories instead of #categories on the document
  //   Using full path with hash causes whole system to refresh. e.g going to https://domain.com#categories would refresh the document instead of just navigating me to that hash
  //   Solution is to add the hash to an anchor tag and call this function in an onclick. This prevents the default action of navigating away and handles the navigation for you
  e.preventDefault();
  document.location.hash = hashLocation;
}
