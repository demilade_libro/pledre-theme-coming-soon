import { Liquid } from "liquidjs";
import * as fs from "fs";

const engine = new Liquid();
const config = JSON.parse(
  await new Promise((res, rej) => {
    fs.readFile("config.json", (err, data) => {
      if (err) {
        console.error(err);
        rej(err);
      }
      res(data);
    });
  })
);
// let mockData
const mockData = JSON.parse(
  await new Promise((res, rej) => {
    fs.readFile("mock-template-options.json", (err, data) => {
      if (err) {
        console.error(err);
        rej(err);
      }
      res(data);
    });
  })
);
const liquidFile = await new Promise((res, rej) => {
  fs.readFile("index.liquid", (err, data) => {
    if (err) {
      console.error(err);
      rej(err);
    }
    res(data);
  });
});

const baseHref = "index.html";

const configTemplateOptions = {};
config.template_groups.forEach((_v) => {
  const keys = _v.keys;
  keys.forEach((_key) => {
    configTemplateOptions[_key.id] = _key.default;
  });
});

// console.log(configTemplateOptions)
engine
  .parseAndRender(liquidFile.toString(), {
    ...mockData,
    ...configTemplateOptions,
    baseHref,
  })
  .then((data) => fs.writeFileSync("index.html", data));
